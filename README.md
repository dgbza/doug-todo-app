# doug-todo-app

## Using the doug-todo-app

1. **Ensure that you have Docker installed on your computer. If not, then check here for the appropriate instructions for your OS:**

https://docs.docker.com/get-docker/


2. **Run the Docker container by typing the following into a terminal/shell:**

docker run -it -p 8000:8000 registry.gitlab.com/dgbza/doug-todo-app:latest


3. **Once the image has completed downloading and the container has started, then browse to**

http://127.0.0.1:8000/login/


4. **Click ‘Sign Up’**


Complete form & click ‘Sign Up’


5. **Login in with the new user’s credentials**


6. **Add a todo by typing in the name of the todo in the text box, followed by clicking ‘Add Todo’**

eg. doug test todo 1


7. **Add another todo by typing in the name of the todo, followed by clicking ‘Add Todo’**

eg. doug test todo 2


8. **To edit a Todo:**

Click the ‘Edit’ button below the Todo that you want to edit

Type the new name of the Todo
	eg. doug test todo 2 amended


Click ‘Submit’


9. **To set a Todo as completed:**

Click the ‘Completed’ link below the Todo that you want to set as completed



10. **To test pagination:**

Add a total of 8 Todos

You should now be able to click the ‘Next’ link at the bottom of the page to show the next 4 Todos

Once it shows the next 4 Todos, you can click the ‘Previous’ link at the bottom of the page to show the previous 4 Todos



11. **To log out:**

Browse to  127.0.0.1:8000

Then click the 'Logout' link in the top right
